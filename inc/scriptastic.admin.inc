<?php
// $Id$

/**
 * @file
 * Administer the Scriptastic module
 */

function scriptastic_admin_settings() {

  $form['revision_thining'] = array(

    '#type' => 'fieldset',
    '#title' => t('Allow the number of revisions to be thinned'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );

  $form['revision_thining']['scriptastic_weeks_keep_all'] = array(

    '#type' => 'select',
    '#title' => t('Number of weeks to keep all revisions for'),
    '#description' => t("All revisions will be keep for this long"),
    '#required' => TRUE,
    '#options' => array(1 => 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 'forever' => 'Forever'),
    '#default_value' => variable_get('scriptastic_weeks_keep_all', 2)
  );

  $form['revision_thining']['scriptastic_weeks_keep_daily'] = array(

    '#type' => 'select',
    '#title' => t('After the above, number of weeks to keep 1 revision a DAY for'),
    '#description' => t("One revision a day will be kept for this period, which starts after the keep all period"),
    '#required' => TRUE,
    '#options' => array(1 => 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 'forever' => 'Forever'),
    '#default_value' => variable_get('scriptastic_weeks_keep_daily', 8)
  );

  $form['revision_thining']['scriptastic_weeks_keep_weekly'] = array(

    '#type' => 'select',
    '#title' => t('After the above, number of weeks to keep 1 revision a WEEK for'),
    '#description' => t("One revision a day will be kept for this period,
                         which starts after the keep all and daily periods.
                         NB. This will mean revisions older this will be deleted."),
    '#required' => TRUE,
    '#options' => array(1 => 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 'forever' => 'Forever'),
    '#default_value' => variable_get('scriptastic_weeks_keep_weekly', 'ignore')
  );

  $form['revision_thining']['scriptastic_use_cron'] = array(

    '#type' => 'select',
    '#title' => t('Enable revision thinning on CRON'),
    '#description' => t("Enable revision thinning based on the above rules, when the cron runs"),
    '#required' => TRUE,
    '#options' => array(0 => 'No', 1 => 'Yes'),
    '#default_value' => variable_get('scriptastic_use_cron', 0)
  );

  $form['revision_thining']['thin'] = array(
    '#type' => 'submit',
    '#value' => t('Thin revisions based on the above'),
    '#submit' => array('scriptastic_thin_revisions_submit'),
  );

  $form['cache'] = array(
    '#type' => 'fieldset',
    '#title' => t('Caching'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );

  $form['cache']['scriptastic_clear_caches'] = array(
    '#type' => 'checkbox',
    '#title' => t('Clear caches for preprocessed scriptastic nodes'),
    '#description' => t("If checked the CSS, JS & page caches will be cleared when <em>preprocessed</em> scriptastic nodes are created / updated."),
    '#default_value' => variable_get('scriptastic_clear_caches', TRUE)
  );

  $form['cache']['scriptastic_rotate_query_string'] = array(
    '#type' => 'checkbox',
    '#title' => t('Rotate JS/CSS Query string'),
    '#description' => t("If checked the CSS & JS query string will be rotated when <em>all</em> scriptastic nodes are created / updated."),
    '#default_value' => variable_get('scriptastic_rotate_query_string', TRUE)
  );

  $form['scriptastic_revision_moderation'] = array(

    '#type' => 'select',
    '#title' => t('Turn on revision moderation'),
    '#description' => t("Enable revision moderation, which will allow future revisions of scripts to be created and used instead of the current revision - requires the Revision Moderation module"),
    '#required' => TRUE,
    '#options' => array(0 => 'No', 1 => 'Yes'),
    '#default_value' => variable_get('scriptastic_revision_moderation', 0)
  );

  return system_settings_form($form);
}

/**
 * Implementation of hook_submit().
 */
function scriptastic_thin_revisions_submit($form, &$form_state) {

  //Process the settings form
  system_settings_form_submit($form, $form_state);

  //Thin revisions, if required
  _scriptastic_thin_revisions();
}
