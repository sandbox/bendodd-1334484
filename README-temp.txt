// $Id$

-- SUMMARY --
Allows the embedding of css and js scripts on pages, physical files are created.  But revision moderated scripts are stored in the database, so moving servers is as easy as clicking a 'recreate scripts' button.

-- REQUIREMENTS --

Required Modules: content, token, pathauto

-- INSTALLATION --
1. Put the 'scriptastic' folder into your modules directory
2. Enable 'scriptastic in 'admin/build/modules' and all required modules
3. Set the relevant 'scriptastic module' permissions

-- CONFIGURATION --

Once installed there is no real configuration, you can just start creating your new scripts.  To do so:
1. Go to 'node/add/scriptastic'
2. Fill out the fields
  * 'Title' - The name of the script (no file extensions here)
  * 'Add the css on specific pages' - this is identical to the way blocks are added to pages.  
  * 'Script type' - Is it javascript or css (screen, print, or all)
  * 'Script' - The actual script to go in the file, treat as a .css or .js file (so no need to <script> or <style> tags)   
  * 'Load Weight' - The scripts will get added in a specific order to each page, so where does this get rendered in the grand scheme of things.
  * 'Themes Access' - Which themes do you want to use this script     
nb. Don't sent 'URL Path' as this will be automated, although you can change it afterwards if required.
3. Set revision and publishing settings
4. Submit

-- CUSTOMIZATION --

-- TROUBLESHOOTING --

-- FAQ --

-- CONTACT --
